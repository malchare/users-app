### Project name

Users

### Prerequisites

Install node

### Running project

Enter the command:

npm install  
npm start

### Description

The Users app displays users in table by fetching data json from webpage. We can add new users and remove existing users from the list in table. The application uses ReactJS

### Author

MC
